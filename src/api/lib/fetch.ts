import axios from 'axios';

const url = '';

export const fetchFunc = async () => {
  try {
    const { data } = await axios.get(url, {
      params: { timeStamp: new Date().getTime() },
    });

    return data;
  } catch (error) {
    console.log(error);
  }
};

import { useState, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
// import millify from "millify";
import { Card, Row, Col, Input, Typography } from "antd";

// import { useGetCryptosQuery } from "../services/cryptoApi";
import { useGetCryptosQuery } from "../services/cryptoApiRoutes";

import { Loading, MiniLineChart } from "../components";
import styles from "./cryptocurrencies.module.css";

const { Title } = Typography;

const Cryptcurrencies = ({ simplified }) => {
  const count = simplified ? 10 : 100;

  const { data: cryptosList, isFetching } = useGetCryptosQuery(count);

  const [cryptos, setCryptos] = useState([]);

  useEffect(() => {
    setCryptos(cryptosList);
  }, [cryptosList]);

  const searchTermHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const filteredData = cryptosList?.filter((coin: { name: string }) =>
      coin.name.toLowerCase().includes(e.target.value.toLowerCase())
    );

    setCryptos(filteredData);
  };

  if (isFetching) return <Loading />;

  console.log(cryptos);

  const coloredNumber = (number: number): boolean => number > 0;

  const formattedPrice = (price: number) => {
    const formattedPrice = price.toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
    });
    return formattedPrice;
  };

  const numberStyle = (number: number) => {
    const background = number > 0 ? "lightblue" : "pink";
    return { background, padding: "0 3px" };
  };

  return (
    <div className={styles.container}>
      {!simplified && (
        <div className={styles.search_crypto}>
          <Input
            placeholder="Search Cryptocurrency"
            onChange={(e) => searchTermHandler(e)}
          />
        </div>
      )}
      <Row gutter={[24, 24]} className={styles.crypto_card_container}>
        {cryptos?.map((currency, i) => (
          <Col
            xs={24}
            sm={12}
            md={8}
            lg={6}
            key={currency.id}
            className={styles.crypto_card}
          >
            <Link href={`/cryptodetails?id=${currency.id}`}>
              <a>
                <Card
                  size="small"
                  title={
                    <Title level={3}>
                      {currency.rank}. {currency.name}
                    </Title>
                  }
                  extra={
                    <Image
                      src={currency.iconUrl}
                      alt={currency.name}
                      width={40}
                      height={40}
                    />
                  }
                  hoverable
                  className={styles.card}
                >
                  <p>Price: {formattedPrice(+currency.price)}</p>
                  {/* 
                  <p>
                    Market Cap: ${millify(currency.marketCap, { precision: 1 })}
                  </p>
                  */}
                  <p>
                    Change (24h):&nbsp;
                    <span style={numberStyle(+currency.priceChange)}>
                      {+currency.priceChange > 0 && "+"}
                      {formattedPrice(+currency.priceChange)}
                    </span>
                    &nbsp; (
                    <span
                      style={
                        coloredNumber(+currency.change)
                          ? { color: "blue" }
                          : { color: "red" }
                      }
                    >
                      {+currency.change > 0 && "+"}
                      {currency.change}%
                    </span>
                    )
                  </p>
                  <div className={styles.chart_container}>
                    <MiniLineChart coinHistory={currency.history} />
                    {/*
                     */}
                  </div>
                </Card>
              </a>
            </Link>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default Cryptcurrencies;

import { useState } from "react";
//import Image from 'next/image';
import { Image } from "antd";
import { Select, Typography, Row, Col, Avatar, Card } from "antd";
import moment from "moment";

// import { useGetCryptosQuery } from '../services/cryptoApi';
import { useGetCryptosQuery } from "../services/cryptoApiRoutes";
// import { useGetCryptoNewsQuery } from '../services/cryptoNewsApi';
import { useGetCryptoNewsQuery } from "../services/cryptoNewsApiRoutes";
import { Loading } from "../components";

import styles from "./news.module.css";

const { Text, Title } = Typography;
const { Option } = Select;

const News = ({ simplified }) => {
  const [newsCategory, setNewsCategory] = useState("Cryptocurrency");

  const { data: cryptoNews } = useGetCryptoNewsQuery({
    newsCategory,
    count: 18,
    // count: simplified ? 6 : 18,
  });
  const { data: cryptos } = useGetCryptosQuery(100);

  if (!cryptoNews?.articles) return <Loading />;
  console.log(cryptoNews);

  const sortData = (data: any, name: string) => {
    let sortedData = [...data];
    console.log(data);

    return sortedData.sort((a: any, b: any) => (a[name] > b[name] ? -1 : 1));
  };

  return (
    <Row gutter={[12, 12]}>
      {!simplified && (
        <Col span={24}>
          <Select
            showSearch
            className={styles.select_news}
            placeholder="Select a Crypto"
            optionFilterProp="children"
            onChange={(value) => setNewsCategory(value as string)}
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="Cryptocurrency">Cryptocurrency</Option>
            {cryptos?.map(({ name }) => (
              <Option key={name} value={name}>
                {name}
              </Option>
            ))}
          </Select>
        </Col>
      )}
      {cryptoNews?.articles &&
        sortData(cryptoNews.articles, "published_date").map(
          (news, i) =>
            (!simplified || (simplified && i <= 6)) && (
              <>
                <Col xs={24} sm={12} lg={8} key={i}>
                  <Card hoverable className={styles.news_card}>
                    <a href={news.link} target="_blank" rel="noreferrer">
                      <div className={styles.news_title_image_container}>
                        <Title level={4} className={styles.news_title}>
                          {news.title}
                        </Title>
                        <div className={styles.news_image_container}>
                          <Image
                            src={news?.media}
                            alt={news.title}
                            width={200}
                          />
                        </div>
                      </div>
                      <p className={styles.news_summary}>
                        {news.summary > 50
                          ? `${news.summary.substring(0, 50)}...`
                          : news.summary}
                      </p>
                      <div className={styles.provider_container}>
                        <div>
                          <Text>{news.clean_url}</Text>
                        </div>
                        <Text>{moment.utc(news.published_date).fromNow()}</Text>
                      </div>
                    </a>
                  </Card>
                </Col>
              </>
            )
        )}
    </Row>
  );
};

export default News;

import { useState } from "react";
import HTMLReactParser from "html-react-parser";
import { useRouter } from "next/router";
import millify from "millify";

// import {
//   useGetCryptoDetailsQuery,
//   useGetCryptoHistoryQuery,
// } from '../services/cryptoApi';
import {
  useGetCryptoDetailsQuery,
  useGetCryptoHistoryQuery,
} from "../services/cryptoApiRoutes";
import { Loading, LineChart } from "../components";

import { Col, Row, Typography, Select } from "antd";
import {
  MoneyCollectOutlined,
  DollarCircleOutlined,
  FundOutlined,
  ExclamationCircleOutlined,
  StopOutlined,
  TrophyOutlined,
  CheckOutlined,
  NumberOutlined,
  ThunderboltOutlined,
} from "@ant-design/icons";

import styles from "./cryptodetails.module.css";

const { Title, Text } = Typography;
const { Option } = Select;

const CryptoDetails = () => {
  const {
    query: { id: coinId },
  } = useRouter();
  const [timePeriod, setTimePeriod] = useState("7d");
  const { data, isFetching } = useGetCryptoDetailsQuery(coinId);
  const { data: coinHistory } = useGetCryptoHistoryQuery({
    coinId,
    timePeriod,
  });

  const cryptoDetails = data?.data?.coin;
  console.log(coinHistory);

  if (isFetching) return <Loading />;

  const time = ["3h", "24h", "7d", "30d", "1y", "3m", "3y", "5y"];

  const stats = [
    {
      title: "Price to USD",
      value: `$ ${cryptoDetails?.price && millify(cryptoDetails.price)}`,
      icon: <DollarCircleOutlined />,
    },
    { title: "Rank", value: cryptoDetails?.rank, icon: <NumberOutlined /> },
    {
      title: "24h Volume",
      value: `$ ${cryptoDetails?.volume && millify(cryptoDetails.volume)}`,
      icon: <ThunderboltOutlined />,
    },
    {
      title: "Market Cap",
      value: `$ ${
        cryptoDetails?.marketCap && millify(cryptoDetails.marketCap)
      }`,
      icon: <DollarCircleOutlined />,
    },
    {
      title: "All-time-high (daily avg.)",
      value: `$ ${
        cryptoDetails?.allTimeHigh?.price &&
        millify(cryptoDetails.allTimeHigh.price)
      }`,
      icon: <TrophyOutlined />,
    },
  ];

  const genericStats = [
    {
      title: "Number Of Markets",
      value: cryptoDetails?.numberOfMarkets,
      icon: <FundOutlined />,
    },
    {
      title: "Number Of Exchanges",
      value: cryptoDetails?.numberOfExchanges,
      icon: <MoneyCollectOutlined />,
    },
    {
      title: "Aprroved Supply",
      value: cryptoDetails?.approvedSupply ? (
        <CheckOutlined />
      ) : (
        <StopOutlined />
      ),
      icon: <ExclamationCircleOutlined />,
    },
    {
      title: "Total Supply",
      value: `$ ${
        cryptoDetails?.totalSupply && millify(cryptoDetails.totalSupply)
      }`,
      icon: <ExclamationCircleOutlined />,
    },
    {
      title: "Circulating Supply",
      value: `$ ${
        cryptoDetails?.circulatingSupply &&
        millify(cryptoDetails.circulatingSupply)
      }`,
      icon: <ExclamationCircleOutlined />,
    },
  ];

  return (
    <Col className={styles.coin_detail_container}>
      <Col className={styles.coin_heading_container}>
        <Title level={2} className={styles.coin_name}>
          {cryptoDetails?.name} ({cryptoDetails?.slug}) Price
        </Title>
        <p>
          {cryptoDetails?.name} live price in US dollars. view value statistics,
          market cap and supply.
        </p>
      </Col>
      <Select
        defaultValue="7d"
        className={styles.select_timeperiod}
        placeholder="Select Time Period"
        onChange={(value) => setTimePeriod(value)}
      >
        {time.map((date) => (
          <Option key={date} value={date}>
            {date}
          </Option>
        ))}
      </Select>
      <LineChart
        coinHistory={coinHistory}
        currentPrice={cryptoDetails.price}
        coinName={cryptoDetails.name}
      />
      <Col className={styles.stats_container}>
        <Col className={styles.coin_value_statistics}>
          <Col className={styles.coin_value_statistics_heading}>
            <Title level={3} className={styles.coin_details_heading}>
              {cryptoDetails?.name} value Statistics
            </Title>
            <p>An overview showing the stats of {cryptoDetails?.name}</p>
          </Col>
          {stats.map(({ icon, title, value }) => (
            <Col key={title} className={styles.coin_stats}>
              <Col className={styles.coin_stats_name}>
                <Text>{icon}</Text>
                <Text>{title}</Text>
              </Col>
              <Text className={styles.stats}>{value}</Text>
            </Col>
          ))}
        </Col>
        <Col className={styles.other_stats_info}>
          <Col className={styles.coin_value_statistics_heading}>
            <Title level={3} className={styles.coin_details_heading}>
              Other value Statistics
            </Title>
            <p>An overview showing the stats of all cryptocurrencies</p>
          </Col>
          {genericStats.map(({ icon, title, value }) => (
            <Col key={title} className={styles.coin_stats}>
              <Col className={styles.coin_stats_name}>
                <Text>{icon}</Text>
                <Text>{title}</Text>
              </Col>
              <Text className={styles.stats}>{value}</Text>
            </Col>
          ))}
        </Col>
      </Col>
      <Col className={styles.coin_desc_link}>
        <Row className={styles.coin_desc}>
          <Title level={3} className={styles.coin_details_heading}>
            What is {cryptoDetails.name}
          </Title>
          {HTMLReactParser(cryptoDetails.description)}
        </Row>
        <Col className={styles.coin_links}>
          <Title level={3} className={styles.coin_details_heading}>
            {cryptoDetails.name} Links
          </Title>
          {cryptoDetails.links.map((link) => (
            <Row key={link.name} className={styles.coin_link}>
              <Title level={4} className={styles.link_name}>
                {link.type}
                <a href={link.url} target="_blank" rel="noreferrer">
                  {link.name}
                </a>
              </Title>
            </Row>
          ))}
        </Col>
      </Col>
    </Col>
  );
};

export default CryptoDetails;

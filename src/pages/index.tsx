import Link from "next/link";
// import millify from "millify";
import { Typography, Row, Col, Statistic } from "antd";

import { useGetCryptosQuery } from "../services/cryptoApiRoutes";

import Cryptocurrencies from "./cryptocurrencies";
import News from "./news";
import { Loading, GlobalStats } from "../components";

import styles from "./index.module.css";

const { Title } = Typography;

const Home: React.FC = () => {
  const { data, isFetching } = useGetCryptosQuery(10);
  const globalStats = data?.data?.stats;

  console.log(data);
  if (isFetching) return <Loading />;

  return (
    <>
      {/* <GlobalStats /> */}
      <div className={styles.home_heading_container}>
        <Title level={3} className={styles.home_title}>
          Top 10 Cryptocurrencies
        </Title>
        <Title level={4} className={styles.show_more}>
          <Link href="/cryptocurrencies">
            <a style={{ color: "darkble" }}>Show More</a>
          </Link>
        </Title>
      </div>
      <Cryptocurrencies simplified />
      <div className={styles.home_heading_container}>
        <Title level={3} className={styles.home_title}>
          Latest Crypto News
        </Title>
        <Title level={4} className={styles.show_more}>
          <Link href="/news">
            <a style={{ color: "darkble" }}>Show More</a>
          </Link>
        </Title>
      </div>
      <News simplified />
    </>
  );
};

export default Home;

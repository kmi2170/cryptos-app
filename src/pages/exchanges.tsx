import millify from "millify";
import HTMLReactParser from "html-react-parser";

import { Collapse, Row, Col, Typography, Avatar } from "antd";

// import { useGetExchangesQuery } from '../services/cryptoApi';
import { useGetExchangesQuery } from "../services/cryptoApiRoutes";
import { Loading } from "../components";

import styles from "./exchanges.module.css";

const { Text } = Typography;
const { Panel } = Collapse;

const Exchanges = () => {
  const { data, isFetching } = useGetExchangesQuery(null);
  const exchangeList = data?.data?.exchanges;
  console.log(data);

  if (isFetching) return <Loading />;

  return (
    <>
      <Row>
        <Col span={6}>Exchanges</Col>
        <Col span={6}>24h Trade Volume</Col>
        <Col span={6}>Markets</Col>
        <Col span={6}>Change</Col>
      </Row>
      <Row>
        {exchangeList.map((exchange) => (
          <Col span={24} key={exchange.id}>
            <Collapse>
              <Panel
                key={exchange.id}
                showArrow={false}
                header={
                  <Row>
                    <Col span={6}>
                      <Text>
                        <strong>{exchange.rank}</strong>
                      </Text>
                      <Avatar
                        src={exchange.iconUrl}
                        className={styles.exchange_image}
                      />
                      <Text>
                        <strong>{exchange.name}</strong>
                      </Text>
                    </Col>
                    <Col span={6}>${millify(exchange.volume)}</Col>
                    <Col span={6}>${millify(exchange.numberOfMarkets)}</Col>
                    <Col span={6}>${millify(exchange.marketShare)}</Col>
                  </Row>
                }
                className={styles.panel}
              >
                {HTMLReactParser(exchange.description || "")}
              </Panel>
            </Collapse>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default Exchanges;

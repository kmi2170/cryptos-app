import { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";

const headers = {
  "x-rapidapi-host": process.env.NEXT_PUBLIC_X_RAPID_API_HOST_COIN_RANKING,
  "x-rapidapi-key": process.env.NEXT_PUBLIC_X_RAPID_API_KEY,
};

const url = "https://coinranking1.p.rapidapi.com/coins";

export default async function coins(req: NextApiRequest, res: NextApiResponse) {
  try {
    const { data } = await axios(url, { params: req.query, headers });

    const priceChangeList = data?.data?.coins.map(({ id }) =>
      coinPriceChange(id, "24h")
    );

    const history24h = data?.data?.coins.map(({ id }) =>
      coinHistory(id, "24h")
    );

    const resolvedPriceChangeList = await Promise.all(priceChangeList);
    const resolvedHistory24h = await Promise.all(history24h);

    const modifiedData = data?.data?.coins.map((coin, i: number) => ({
      ...coin,
      priceChange: resolvedPriceChangeList[i],
      history24h: resolvedHistory24h[i],
    }));

    res.status(200).json(modifiedData);
  } catch (error) {
    res.status(500).json(error);
  }
}

const url_h = "https://coinranking1.p.rapidapi.com/coin";

const coinPriceChange = async (coinId: string, timePeriod: string) => {
  try {
    const { data } = await axios(`${url_h}/${coinId}/history/${timePeriod}`, {
      headers,
    });

    const history = data?.data?.history;
    const priceChange = +history[history.length - 1].price - +history[0].price;

    return priceChange;
  } catch (error) {
    console.log(error);
  }
};

const coinHistory = async (coinId: string, timePeriod: string) => {
  try {
    const { data } = await axios(`${url_h}/${coinId}/history/${timePeriod}`, {
      headers,
    });

    const history = data?.data?.history;

    return history;
  } catch (error) {
    console.log(error);
  }
};

// export default async function coins(req: NextApiRequest, res: NextApiResponse) {
//   const { data } = await axios(url, { params: req.query, headers });

//   // res.status(200).json({ test: 'test response' });
//   res.status(200).json(data);
// }

import { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';

const headers = {
  'x-rapidapi-host': process.env.NEXT_PUBLIC_X_RAPID_API_HOST_COIN_RANKING,
  'x-rapidapi-key': process.env.NEXT_PUBLIC_X_RAPID_API_KEY,
};

const url = 'https://coinranking1.p.rapidapi.com/coin';

export default async function coins(req: NextApiRequest, res: NextApiResponse) {
  try {

    const { coinId, timePeriod } = req.query;

    const { data } = await axios(`${url}/${coinId}/history/${timePeriod}`, {
      headers,
    });

    res.status(200).json(data);
  } catch (error) {
    res.status(500).json(error);
  }
}

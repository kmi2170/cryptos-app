import { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";

const headers = {
  "x-rapidapi-host": process.env.NEXT_PUBLIC_X_RAPID_API_HOST_FREE_NEWS,
  "x-rapidapi-key": process.env.NEXT_PUBLIC_X_RAPID_API_KEY,
};

const url = "https://free-news.p.rapidapi.com/v1/search";

export default async function coins(req: NextApiRequest, res: NextApiResponse) {
  try {
    const { data } = await axios(url, { params: req.query, headers });

    res.status(200).json(data);
  } catch (error) {
    res.status(500).json(error);
  }
}

import { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";

const headers = {
  "x-rapidapi-host": process.env.NEXT_PUBLIC_X_RAPID_API_HOST_COIN_RANKING,
  "x-rapidapi-key": process.env.NEXT_PUBLIC_X_RAPID_API_KEY,
};

const url = "https://coinranking1.p.rapidapi.com/coins";

export default async function coins(req: NextApiRequest, res: NextApiResponse) {
  try {
    const { data } = await axios(url, { params: req.query, headers });

    const modifiedData = data?.data?.coins.map(
      (coin: { history: string[] }) => {
        const history = coin.history;
        const priceChange = +history[history.length - 1] - +history[0];

        return { ...coin, priceChange };
      }
    );

    res.status(200).json(modifiedData);
  } catch (error) {
    res.status(500).json(error);
  }
}

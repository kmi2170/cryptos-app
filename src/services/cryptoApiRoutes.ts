import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const cryptoApiRoutes = createApi({
  reducerPath: "cryptoApiRoutes",
  baseQuery: fetchBaseQuery({ baseUrl: "/api" }),
  refetchOnMountOrArgChange: 30,
  refetchOnFocus: true,
  refetchOnReconnect: true,
  endpoints: (builder) => ({
    getCryptos: builder.query({
      query: (count) => `/coins?limit=${count}`,
    }),
    getCryptosExtended: builder.query({
      query: (count) => `/coins_extended?limit=${count}`,
    }),
    getExchanges: builder.query({
      query: () => `/exchanges`,
    }),
    getCryptoDetails: builder.query({
      query: (coinId) => `/coin/${coinId}`,
    }),
    getCryptoHistory: builder.query({
      query: ({ coinId, timePeriod }) =>
        `/coin/${coinId}/history/${timePeriod}`,
    }),
  }),
});

export const {
  useGetCryptosQuery,
  useGetCryptosExtendedQuery,
  useGetExchangesQuery,
  useGetCryptoDetailsQuery,
  useGetCryptoHistoryQuery,
} = cryptoApiRoutes;

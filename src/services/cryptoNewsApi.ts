import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const cryptoNewsApiHeaders = {
  'x-rapidapi-host': process.env.NEXT_PUBLIC_X_RAPID_API_HOST_FREE_NEWS,
  'x-rapidapi-key': process.env.NEXT_PUBLIC_X_RAPID_API_KEY,
};

const baseUrl = 'https://free-news.p.rapidapi.com/v1';

const createRequest = (url: string) => ({
  url,
  headers: cryptoNewsApiHeaders,
});

export const cryptoNewsApi = createApi({
  reducerPath: 'cryptoNewsApi',
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    getCryptoNews: builder.query({
      query: ({ newsCategory, count }) =>
        createRequest(`/search?q=${newsCategory}&page_size=${count}`),
    }),
  }),
});

export const { useGetCryptoNewsQuery } = cryptoNewsApi;

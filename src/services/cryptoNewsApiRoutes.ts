import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const cryptoNewsApiRoutes = createApi({
  reducerPath: "cryptoNewsApiRoutes",
  baseQuery: fetchBaseQuery({ baseUrl: "/api" }),
  refetchOnMountOrArgChange: 30,
  endpoints: (builder) => ({
    getCryptoNews: builder.query({
      query: ({ newsCategory, count }) =>
        `/news?q=${newsCategory}&page_size=${count}`,
    }),
  }),
});

export const { useGetCryptoNewsQuery } = cryptoNewsApiRoutes;

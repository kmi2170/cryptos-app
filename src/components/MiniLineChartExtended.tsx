import { Line } from "react-chartjs-2";

const LineChart = ({ coinHistory }) => {
  const coinPrice = [];
  const coinTimeStamp = [];

  for (let i = 0; i < coinHistory?.length; i++) {
    coinPrice.push(coinHistory[i].price);
    coinTimeStamp.push(new Date(coinHistory[i].timestamp).toLocaleString());
  }

  const coinPriceMax = Math.max(...coinHistory);
  const coinPriceMin = Math.min(...coinHistory);

  const data = {
    labels: coinTimeStamp,
    datasets: [
      {
        data: coinPrice,
        full: false,
        backgroundColor: "#0071bd",
        borderColor: "#0071bd",
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRation: false,
    scales: {
      x: {
        ticks: { display: false },
        grid: { display: false },
      },
      y: {
        grid: { display: false },
        tickes: {
          suggestedMax: coinPriceMax,
          suggestedMin: coinPriceMin,
        },
      },
    },
    elements: { point: { radius: 0 }, line: { borderWidth: 2 } },
    plugins: {
      legend: {
        display: false,
      },
    },
  };

  return (
    <>
      <Line data={data} options={options} />
    </>
  );
};

export default LineChart;

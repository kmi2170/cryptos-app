import { Line } from "react-chartjs-2";
import { Col, Row, Typography } from "antd";
import millify from "millify";

import styles from "./LineChart.module.css";

const { Title } = Typography;

const LineChart = ({ coinHistory, currentPrice, coinName }) => {
  const coinPrice = [];
  const coinTimeStamp = [];

  const priceChange =
    +coinHistory?.data?.history[coinHistory.data.history.length - 1].price -
    +coinHistory?.data?.history[0].price;

  // console.log(coinHistory?.data?.history[0]);
  // console.log(coinHistory?.data?.history[coinHistory.data.history.length - 1]);

  for (let i = 0; i < coinHistory?.data?.history.length; i++) {
    coinPrice.push(coinHistory.data.history[i].price);
    coinTimeStamp.push(
      new Date(coinHistory.data.history[i].timestamp).toLocaleDateString()
    );
  }

  const data = {
    labels: coinTimeStamp,
    datasets: [
      {
        label: "Price in USD",
        data: coinPrice,
        full: false,
        backgroundColor: "#0071bd",
        borderColor: "#0071bd",
      },
    ],
  };

  const options = {
    responsive: true,
    scales: {
      y: {
        // beginAtZero: true,
      },
    },
  };

  const coloredNumber = (number: number): boolean => number > 0;

  const formattedPrice = (price: number) => {
    const formattedPrice = price.toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
    });
    return formattedPrice;
  };

  const numberStyle = (number: number) => {
    const background = number > 0 ? "lightblue" : "pink";
    return { background, padding: "0 3px" };
  };

  return (
    <>
      <Row className={styles.chart_header}>
        <Title level={2} className={styles.chart_title}>
          {coinName} Price Chart
        </Title>
        <Col className={styles.price_container}>
          <Title level={3} className={styles.price_change}>
            Change:&nbsp;
            <span style={numberStyle(+priceChange)}>
              {formattedPrice(+priceChange)}
            </span>
            &nbsp; (
            <span
              style={
                coloredNumber(+priceChange)
                  ? { color: "blue" }
                  : { color: "red" }
              }
            >
              {millify(coinHistory?.data?.change, { precision: 2 })}
            </span>
            % )
          </Title>
          <Title level={3} className={styles.current_price}>
            Current {coinName} Price: {formattedPrice(+currentPrice)}
          </Title>
        </Col>
      </Row>
      <Line data={data} options={options} />
    </>
  );
};

export default LineChart;

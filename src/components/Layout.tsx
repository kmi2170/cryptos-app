import React from 'react';
import { Navbar, Footer } from '.';

import styles from './Layout.module.css';

type LayoutProps = {
  children: React.ReactNode;
};

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div className={styles.app}>
      <div className={styles.navbar}>
        <Navbar />
      </div>
      <main className={styles.main}>
        <div className={styles.children}>{children}</div>
        <Footer />
      </main>
    </div>
  );
};

export default Layout;

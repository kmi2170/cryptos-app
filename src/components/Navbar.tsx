import { useState, useEffect } from "react";
import Link from "next/link";

import { Button, Menu, Typography, Avatar } from "antd";

// import icon from '../../public/images/Dollar-Sign-icon.png';

import {
  HomeOutlined,
  MoneyCollectOutlined,
  BulbOutlined,
  FundOutlined,
  MenuOutlined,
} from "@ant-design/icons";

import styles from "./Navbar.module.css";

const Navbar = () => {
  const [activeMenu, setActiveMenu] = useState(true);
  const [screenSize, setScreenSize] = useState(null);

  useEffect(() => {
    const handleResize = () => setScreenSize(window.innerWidth);

    window.addEventListener("resize", handleResize);

    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    if (screenSize < 800) {
      setActiveMenu(false);
    } else {
      setActiveMenu(true);
    }
  }, [screenSize]);

  return (
    <div className={styles.nav_container}>
      <div className={styles.logo_container}>
        {/* 
        <Avatar src={icon.src} size="large" />
      */}
        <Typography.Title level={2}>
          <Link href="/">
            <a className={styles.link}>CryptosInfo</a>
          </Link>
        </Typography.Title>
        <Button
          className={styles.menu_control_container}
          onClick={() => setActiveMenu((prev) => !prev)}
        >
          <MenuOutlined />
        </Button>
      </div>
      {activeMenu && (
        <Menu theme="light" className={styles.active_menu}>
          <Menu.Item
            key="home"
            icon={<HomeOutlined />}
            className={styles.menu_item}
          >
            <Link href="/">
              <a className={styles.menu_item}>Home</a>
            </Link>
          </Menu.Item>
          <Menu.Item
            key="cryptocurrencies"
            icon={<FundOutlined />}
            className={styles.menu_item}
          >
            <Link href="/cryptocurrencies">
              <a className={styles.menu_item}>Cryptcurrencies</a>
            </Link>
          </Menu.Item>
          <Menu.Item
            key="exchanges"
            icon={<MoneyCollectOutlined />}
            className={styles.menu_item}
          >
            <Link href="/exchanges">
              <a className={styles.menu_item}>Exchanges</a>
            </Link>
          </Menu.Item>
          <Menu.Item
            key="news"
            icon={<BulbOutlined />}
            className={styles.menu_item}
          >
            <Link href="/news">
              <a className={styles.menu_item}>News</a>
            </Link>
          </Menu.Item>
        </Menu>
      )}
    </div>
  );
};

export default Navbar;

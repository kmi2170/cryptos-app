import { Spin } from 'antd';

import styles from './Loading.module.css';

const Loader = () => {
  return (
    <div className={styles.loading}>
      <Spin />
    </div>
  );
};

export default Loader;

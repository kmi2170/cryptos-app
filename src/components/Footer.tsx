import Link from "next/link";

import { Typography, Space } from "antd";

const Footer = () => {
  const dt = new Date();
  const year = dt.getFullYear();

  return (
    <footer
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        background: "black",
        padding: "0.5rem 0",
      }}
    >
      <Typography.Title
        level={5}
        style={{ textAlign: "center", color: "white" }}
      >
        Copyrihgt &copy; kmi {year}. All rights reserved. | Powered by
        Coinrainking and Newscatcher.
      </Typography.Title>

      <Space>
        <Link href="/">Home</Link>
        <Link href="/cryptcurrencies">Cryptcurrencies</Link>
        <Link href="/exchanges">Exchanges</Link>
        <Link href="/news">News</Link>
      </Space>
    </footer>
  );
};

export default Footer;

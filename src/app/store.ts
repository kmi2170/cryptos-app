import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";

import { cryptoApi } from "../services/cryptoApi";
import { cryptoApiRoutes } from "../services/cryptoApiRoutes";
import { cryptoNewsApi } from "../services/cryptoNewsApi";
import { cryptoNewsApiRoutes } from "../services/cryptoNewsApiRoutes";

export const store = configureStore({
  reducer: {
    [cryptoApi.reducerPath]: cryptoApi.reducer,
    [cryptoApiRoutes.reducerPath]: cryptoApiRoutes.reducer,
    [cryptoNewsApi.reducerPath]: cryptoNewsApi.reducer,
    [cryptoNewsApiRoutes.reducerPath]: cryptoNewsApiRoutes.reducer,
  },
});

setupListeners(store.dispatch);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

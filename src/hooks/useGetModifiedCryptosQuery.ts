import {
  useGetCryptosQuery,
  useGetCryptoHistoryQuery,
} from '../services/cryptoApi';

export const useGetModifiedCryptosQuery = (count: number) => {
  const { data: cryptosList, isFetching, error } = useGetCryptosQuery(count);

  const priceChangeList = cryptosList?.data?.coins.map(({ id }) =>
    coinPriceChange(id, '24h')
  );

  const modifiedData = cryptosList?.data?.coins.map((coin, i) => ({
    ...coin,
    priceChange: priceChangeList[i],
  }));

  return { modifiedData, isFetching, error };
};

const coinPriceChange = (coinId: string, timePeriod: string) => {
  const { data } = useGetCryptoHistoryQuery({
    coinId,
    timePeriod,
  });

  const history = data?.data?.history;
  if (history) {
    const priceChange = +history[history.length - 1].price - +history[0].price;

    return priceChange;
  }
};
